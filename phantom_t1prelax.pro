pro phantom_t1prelax, phantom_path
; this script calculates the t1p maps from multi-echo t1p sequence
; for agarose phantoms
; t1ppath points to folder w/ all t1p echoes (84 images)
; saves .tif files w/ t1p maps
; Noah Bonnheim | February 2022

print, '---phantom_t1prelax.pro---'
te = [0.0, 10.0, 12.847, 25.695, 40.0, 51.39, 80.0] ; ucsf echo times (ms)
;te = [0.032, 5.0, 10.2, 15.2, 20.2, 25.2] ; ucsd echo times (ms)
tesize = size(te)
echoes = tesize(1)

tesort = sort(te)
te = te(tesort)

t1ppath = phantom_path + '/t1rho/'

cmdline = 'ls ' + t1ppath + ' | sort -t _ -n -k5'
spawn, cmdline, t1pfns
nfiles = size(t1pfns)
nfiles = nfiles(1)
if nfiles ne 84 then print,  'Error in number of files.' ; should be 84 files

; determine the size of the image matrix
; note that sb_c is the bth slice and the cth echo
s1_1 = read_dicom(t1ppath + t1pfns(0))
s1_1size = size(s1_1)
xs = s1_1size(1)
ys = s1_1size(2)
slices = nfiles / echoes

; create the matrix to store the t1p maps
t1pmaps = dblarr(xs, ys, slices)

; create the matrix to store all of the echoes for each slice
S = dblarr(xs, ys, nfiles)

for i = 0, nfiles-1 do begin
	S(*,*,i) = read_dicom(t1ppath + t1pfns(i))
endfor 

;--- sort images by position in stack and echo ---
; for example, Snew(*,*,0) = image 1, echo 1
;	       Snew(*,*,1) = image 1, echo 2
;	       Snew(*,*,3) = image 1, echo 3
Snew = dblarr(xs,ys,nfiles)
z = 0
for i = 0, slices-1 do begin
	for j = 0, echoes-1 do begin
		Snew(*,*,z) = S(*,*,(tesort(j)*slices) + i)

		; -- this section can be used to confirm order is correct
		; -- reads dicom meta data, prints echo time
		ci = tesort(j)*slices + i
		obj = obj_new('idlffdicom', t1ppath + t1pfns(ci))
		imte = obj->getvalue('0018'x,'0081'x, /no_copy)
		imte = *imte[0] ; * is a pointer?
;		print, 'image ' + string(ci) + ' | te ' + string(imte)
		;---
		z = z + 1
	endfor
endfor
;---

tic
; calculate t1p maps
nnotconverged = 0 ; initialize
nconverged = 0
for z = 0, slices-1 do begin
	tissue = where( Snew(*,*,z*echoes) gt 200, tcount) ; where SI of 1st echo > 40
	for i = 0, tcount-1 do begin 
		xy = array_indices(Snew(*,*,z*echoes), tissue(i))
	;	A = [1.2, 10.0]
	;	A = [120.0, 10.0]
	;	A = [1200.0, 10.0]
	;	A = [12000.0, 10.0]
		A = [1024.0, 64.0]
		if (z eq 1) and (i eq 1) then begin
			 print ,'initial A: '+string(A[0])+','+ string(A[1]) 
		endif
		Y = Snew(xy(0), xy(1), z*echoes:(z*echoes)+echoes-1)
		tolerance = 1.0
		maxit = 100
		coefs = lmfit(te, Y, A, CONVERGENCE=converge, /DOUBLE, $
			FUNCTION_NAME='t2decay', ITMAX=maxit, TOL=tolerance)	
	
		; calculate n voxels converged and not converged
		idx = where( converge EQ 1, count)
		nconverged = nconverged + count
		idx = where( converge NE 1, count)
		nnotconverged = nnotconverged + count

		;if (converge ne 0) then begin
			t1pmaps(xy(0), xy(1), z) = A[1]
		;endif
	endfor
	progress =  string(round(float(z+1) / float(slices) * 100.0))
	if (z eq 2) or (z eq 5) or (z eq 8) then begin
		print, 'decay fitting ' + progress + '% complete'
	endif
endfor

print, strcompress('n voxels converged: ' + string(nconverged))
print, strcompress('n not converged: ' + string(nnotconverged))
pc = (float(nconverged) / (float(nconverged) + float(nnotconverged))) * 100.0
print, 'percent converged: ' + string(pc) + '%'

; remove infinite values
idx = where( finite(t1pmaps, /infinity), count)
t1pmaps(idx) = 0
print, 'n voxels inf: ' + string(count)

; remove unusually high/low values
idx = where( abs(t1pmaps) gt 500, count)
t1pmaps(idx) = 0
print, 'n voxels > 500 ms: ' + string(count)

print, 'tolerance: ' + string (tolerance)
print, 'max iterations: ' + string (maxit)
; save the maps as floating point TIFFs
mappath = phantom_path + '/maps_t1rho/'
for i = 0, slices-1 do begin
	imagename = 'maps_t1rho_' + string(i+1, format='(I03)') + '.tiff'
	nfn = strcompress(mappath + imagename, /remove_all)
	write_tiff, nfn, t1pmaps(*,*,i), /float
endfor

print, 'T1p maps saved to ' + mappath
print, '------------------'

toc

end
