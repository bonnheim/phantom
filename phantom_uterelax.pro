pro phantom_uterelax, phantom_path
; this script calculates the t2star maps from multi-echo ute sequence
; for agarose phantoms
; utepath points to folder w/ all ute echoes (168 images)
; saves .tif files w/ t2star maps
; Noah Bonnheim | January 2022

print, '---phantom_uterelax.pro---'
te = [0.248, 5.2, 10.2, 15.2, 20.2, 25.2] ; ucsf echo times (ms)
;te = [0.032, 5.0, 10.2, 15.2, 20.2, 25.2] ; ucsd echo times (ms)
tesize = size(te)
echoes = tesize(1)

tesort = sort(te)
te = te(tesort)

utepath = phantom_path + '/ute/'

;cmdline = 'ls ' + sample_path + ' | grep UTE_'
;spawn, cmdline, seqs
;uteseq = seqs(0); UTE sequence
;utepath = sample_path + '/' + uteseq + '/'

; determine subject ID
;sub1 = strsplit(sample_path, '/MRI/', /regex, /extract)
;sub2 = strsplit(sub1[1], '/VISIT_1', /regex, /extract)
;subject = sub2[0]

cmdline = 'ls ' + utepath + ' | sort -t _ -n -k5'
spawn, cmdline, utefns
nfiles = size(utefns)
nfiles = nfiles(1)
if nfiles ne 168 then print,  'Error in number of files.' ; should be 168 files

; determine the size of the image matrix
; note that sb_c is the bth slice and the cth echo
s1_1 = read_dicom(utepath + utefns(0))
s1_1size = size(s1_1)
xs = s1_1size(1)
ys = s1_1size(2)
slices = nfiles / echoes

; create the matrix to store the T2* maps
;t2smaps = fltarr(xs, ys, slices)
t2smaps = dblarr(xs, ys, slices)

; create the matrix to store all of the echoes for each slice
;S = fltarr(xs, ys, nfiles)
S = dblarr(xs, ys, nfiles)

;print, utefns ; verify reading in correct order (1-168)

for i = 0, nfiles-1 do begin
	S(*,*,i) = read_dicom(utepath + utefns(i))
endfor 

;--- sort images by position in stack and echo ---
; for example, Snew(*,*,0) = image 1, echo 1
;	       Snew(*,*,1) = image 1, echo 2
;	       Snew(*,*,3) = image 1, echo 3
;Snew = fltarr(xs, ys, nfiles)
Snew = dblarr(xs,ys,nfiles)
z = 0
for i = 0, slices-1 do begin
	for j = 0, echoes-1 do begin
		Snew(*,*,z) = S(*,*,(tesort(j)*slices) + i)

		; -- this section can be used to confirm order is correct
		; -- reads dicom meta data, prints echo time
		ci = tesort(j)*slices + i
		obj = obj_new('idlffdicom', utepath + utefns(ci))
		imte = obj->getvalue('0018'x,'0081'x, /no_copy)
		imte = *imte[0] ; * is a pointer?
;		print, 'image ' + string(ci) + ' | te ' + string(imte)
		;---
		z = z + 1
	endfor
endfor
;---

tic
; calculate T2* maps
nnotconverged = 0 ; initialize
nconverged = 0
for z = 0, slices-1 do begin
	tissue = where( Snew(*,*,z*echoes) gt 40, count) ; where SI of 1st echo > 40
	for i = 0, count-1 do begin 
		xy = array_indices(Snew(*,*,z*echoes), tissue(i))
		;A = [3000, 10] ; initial guess
		;A = [300, 10] ; t 
		;A = [3000, 100] ; t ~ 210 s
		;A = [300, 100] ; t ~ 179 s 
		;A = [3000, 1000] ; t ~70 s 
		;A = [300, 1000] ; t ~ 90 s 
		;A = [3000, 1]
		;A = [300, 1]
		;A = [30, 1]
		;A = [3, 1]
		;A = [30, 10]
		;A = [30, 100]
		;A = [30, 0.1]
		;A = [3000, 0.1]
		;A = [60, 10]	
		;A = [60, 20]
		;A = [60, 5]
		A = [120.0, 10.0]
		Y = Snew(xy(0), xy(1), z*echoes:(z*echoes)+echoes-1)
		coefs = lmfit(te, Y, A, CONVERGENCE=converge, /DOUBLE, $
			FUNCTION_NAME='t2decay', ITMAX=100, TOL=.1)
		
		; calculate n voxels converged and not converged
		idx = where( converge EQ 1, count)
		nconverged = nconverged + count
		idx = where( converge NE 1, count)
		nnotconverged = nnotconverged + count

		;if (converge ne 10) then begin
		if (converge ge 0) then begin
			t2smaps(xy(0), xy(1), z) = A[1]
		endif
	endfor
	progress =  string(round(float(z+1) / float(slices) * 100.0))
	if (z eq 6) or (z eq 12) or (z eq 18) or (z eq 24) then begin
		print, 'decay fitting ' + progress + '% complete'
	endif
endfor

print, 'n voxels converged: ' + string(nconverged)
print, 'n voxels not converged: ' + string(nnotconverged)

; remove infinite values
idx = where( finite(t2smaps, /infinity), count)
t2smaps(idx) = 0
print, 'n voxels inf: ' + string(count)

; remove unusually high/low values
idx = where( abs(t2smaps) GT 500, count)
t2smaps(idx) = 0
print, 'n voxels > 500 ms: ' + string(count)

; save the maps as floating point TIFFs
mappath = phantom_path + '/maps_t2str/'
for i = 0, slices-1 do begin
	imagename = 'maps_t2str_' + string(i+1, format='(I03)') + '.tiff'
	nfn = strcompress(mappath + imagename, /remove_all)
	write_tiff, nfn, t2smaps(*,*,i), /float
endfor

print, 'T2* maps saved to ' + mappath
print, '------------------'

toc

end
